/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Copyright Evandro Pires Alves - evandro.pa@gmail.com - 2015
*/
#include <stdlib.h>
#include <unistd.h>
#include <ncurses.h>
#include <glib-2.0/glib.h>
#include <libupower-glib/upower.h>

typedef struct t_battery {
	UpDeviceState state;
	gdouble charge_level;
	gdouble energy_level;
	gdouble energy_rate;
	gdouble energy_empty;
	gdouble energy_full;
} t_battery;

t_battery* battery = NULL;

void init() {
	battery = (t_battery*) malloc(sizeof(battery));
	initscr();
}

void finish() {
	endwin();
	free(battery);
}

gdouble battery_read() {
	UpClient* uPclient = up_client_new();
	GPtrArray* devices = up_client_get_devices(uPclient);
	if (!devices) {
		gdouble zero = 0;
		return zero;
	}

	UpDevice* device;
	UpDeviceKind kind;
	UpDeviceState state;
	gboolean is_present;
	gdouble energy, energy_empty, energy_full, energy_rate;
	
	/*
	 * Combined battery data.
	 */
	int num_battery_devices = 0;
	// Set the initial state to unknown - there might be no battery devices.
	UpDeviceState c_state = 0;
	gdouble c_energy = 0, c_energy_full = 0, c_energy_rate = 0;

	/*
	 * Final battery data.
	 */
	UpDeviceState f_state;
	gdouble f_percentage = 0;
	gdouble f_charging_time = 0;
	gdouble f_discharging_time = 0;

	int i;
	for (i = 0; i < devices->len; i++) {
		device = g_ptr_array_index(devices, i);
		g_object_get(device,
			"kind", &kind,
			"state", &state,
			"is-present", &is_present,
			"energy", &energy,
			"energy-empty", &energy_empty,
			"energy-full", &energy_full,
			"energy-rate", &energy_rate, NULL);

		// Only take into account present battery devices
		if (kind != UP_DEVICE_KIND_BATTERY || !is_present) {
			continue;
		}

		num_battery_devices++;

		// Only update the state if the current combined state does not indicate
		// that a battery device is discharging to keep the system running
		//if (c_state != UP_DEVICE_STATE_DISCHARGING)
		//    c_state = state;
		c_state |= (1 << state);

		c_energy += energy - energy_empty;
		c_energy_full += energy_full;
		c_energy_rate += energy_rate;
	}

	g_ptr_array_unref(devices);

	if (c_energy_full > 0) {
		f_percentage = c_energy / c_energy_full;
	}

	if (c_state & (1 << UP_DEVICE_STATE_DISCHARGING)) {
		f_state = UP_DEVICE_STATE_DISCHARGING;
	} else if (c_state & (1 << UP_DEVICE_STATE_CHARGING)) {
		f_state = UP_DEVICE_STATE_CHARGING;
	} else if (c_state & (1 << UP_DEVICE_STATE_FULLY_CHARGED)) {
		f_state = UP_DEVICE_STATE_FULLY_CHARGED;
	} else {
		f_state = UP_DEVICE_STATE_UNKNOWN;
	}

	if (f_state == UP_DEVICE_STATE_DISCHARGING) {
		f_discharging_time = 3600.0 * c_energy / c_energy_rate;
	} else if (f_state == UP_DEVICE_STATE_CHARGING) {
		f_charging_time = 3600.0 * (c_energy_full - c_energy) / c_energy_rate;
	}

	return f_percentage;
}

void createBattery() {
	int x = 3;
	mvprintw(x++, 0, "    ________________");
	mvprintw(x++, 0, " __|                |");
	mvprintw(x++, 0, "|__                 |");
	mvprintw(x++, 0, "   |________________|");
}

void updateBattery(gdouble bl) {
	int x = 3;
	int bp = bl * 100;
	int n = bp / 5;
	int i = 0;
	for (i=0; i<n; i++) {
		if (i < 3) {
			mvprintw(2+x, i+1, "#");
		} else {
			mvprintw(1+x, i+1, "#");
			mvprintw(2+x, i+1, "#");
			mvprintw(3+x, i+1, "#");
		}
	}
	mvprintw(2+x, 6, " %d%% ", bp);
}

void createChart(i) {
	int x = 0;
	int y = 23;
	mvprintw(x++, y, "100%%|                                                            ");
	mvprintw(x++, y, "    |                                                            ");
	mvprintw(x++, y, "    |                                                            ");
	mvprintw(x++, y, "    |                                                            ");
	mvprintw(x++, y, "    |                                                            ");
	mvprintw(x++, y, "    |                                                            ");
	mvprintw(x++, y, "    |                                                            ");
	mvprintw(x++, y, "    |                                                            ");
	mvprintw(x++, y, "    |                                                            ");
	mvprintw(x++, y, "    |                                                            ");
	mvprintw(x++, y, " 50%%|                                                            ");
	mvprintw(x++, y, "    |                                                            ");
	mvprintw(x++, y, "    |                                                            ");
	mvprintw(x++, y, "    |                                                            ");
	mvprintw(x++, y, "    |                                                            ");
	mvprintw(x++, y, "    |                                                            ");
	mvprintw(x++, y, "    |                                                            ");
	mvprintw(x++, y, "    |                                                            ");
	mvprintw(x++, y, "    |                                                            ");
	mvprintw(x++, y, "    |                                                            ");
	mvprintw(x++, y, "  0%%|____________________________________________________________");
}

void updateChart(int hist[60]) {
	int i = 0;
	for (i=0; i<60; i++) {
		mvprintw(20-hist[i]/5, i+28, "*");
	}
}

int main(int argc, char *argv[]) {
	init();
	int hist[60];
	int i = 0;
	for (i=0; i<60; i++) {
		hist[i] = 0;
	}

	while (true) {
		gdouble bl = battery_read();
		// update chart array
		int bp = bl * 100;
		for (i=0; i<59; i++) {
			hist[i] = hist[i+1];
		}
		hist[59] = bp;
		// show info
		createBattery();
		updateBattery(bl);
		createChart();
		updateChart(hist);
		move(0, 0);
		refresh();
		sleep(120);
	}
	getch();

	finish();
	return 0;
}
